<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>网站后台管理系统HTML模板--我爱模板网 www.5imoban.net</title>

<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" />
<script language="JavaScript" src="{{asset('js/jquery.js')}}"></script>

<script type="text/javascript">
$(document).ready(function(){
  $(".click").click(function(){
  $(".tip").fadeIn(200);
  });
  
  $(".tiptop a").click(function(){
  $(".tip").fadeOut(200);
});

  $(".sure").click(function(){
  $(".tip").fadeOut(100);
});

  $(".cancel").click(function(){
  $(".tip").fadeOut(100);
});

});
</script>


</head>


<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">数据表</a></li>
    <li><a href="#">基本内容</a></li>
    </ul>
    </div>
    
    <div class="rightinfo">
    
    <div class="tools">
    
    	<ul class="toolbar">
        <li class="click"><span><img src="{{asset('images/t01.png')}}" /></span>添加</li>
        <li class="click"><span><img  src="{{asset('images/t02.png')}}" /></span>修改</li>
        <li><span><img  src="{{asset('images/t03.png')}}" /></span>删除</li>
        <li><span><img  src="{{asset('images/t04.png')}}" /></span>统计</li>
        </ul>
        
        
        <ul class="toolbar1">
        <li><span><img  src="{{asset('images/t05.png')}}" /></span>设置</li>
        </ul>
    
    </div>
    
    
    <table class="tablelist">
    	<thead>
    	<tr>
        <th><input name="" type="checkbox" value="" checked="checked"/></th>
        <th>文章id<i class="sort"><img  src="{{asset('images/px.gif')}}" /></i></th>
        <th>标题</th>
        <th>用户</th>
        <th>发布时间</th>

        <th>操作</th>
        </tr>
        </thead>
        <tbody>
        @if($data)
            @foreach($data as $v)
                <tr>
                    <td><input name="" type="checkbox" value="" /></td>
                    <td>{{$v['user_id']}}</td>
                    <td>{{$v['user_name']}}</td>
                    <td><a href="/user/showauths/{{$v['user_id']}}" class="tablelink">查看权限</a>     <a href="/article/edit/{{$v['user_id']}}" class="tablelink">编辑</a>   <a href="/article/delete/{{$v['user_id']}}" class="tablelink"> 删除</a></td>
                 </tr>
             @endforeach
        @endif

        </tbody>
    </table>
    
   
    <div class="pagin">
    	<div class="message">共<i class="blue">10</i>条记录，当前显示第&nbsp;<i class="blue">&nbsp;</i>页</div>
     <!--  <ul class="paginList">
        <li class="paginItem"><a href="javascript:;"><span class="pagepre"></span></a></li>
        <li class="paginItem"><a href="javascript:;">1</a></li>
        <li class="paginItem current"><a href="javascript:;">2</a></li>
        <li class="paginItem"><a href="javascript:;">3</a></li>
        <li class="paginItem"><a href="javascript:;">4</a></li>
        <li class="paginItem"><a href="javascript:;">5</a></li>
        <li class="paginItem more"><a href="javascript:;">...</a></li>
        <li class="paginItem"><a href="javascript:;">10</a></li>
        <li class="paginItem"><a href="javascript:;"><span class="pagenxt"></span></a></li>
        </ul>-->

    </div>
        <script>
            $('ul.pager').addClass('paginList');
            $('ul.pager li').addClass('paginItem');
            $('ul.pager li a:first()').addClass('pagepre');
            $('ul.pager li a:last()').addClass('pagenxt');
        </script>

    
    <div class="tip">
    	<div class="tiptop"><span>提示信息</span><a></a></div>
        
      <div class="tipinfo">
        <span><img  src="{{asset('images/ticon.png')}}" /></span>
        <div class="tipright">
        <p>是否确认对信息的修改 ？</p>
        <cite>如果是请点击确定按钮 ，否则请点取消。</cite>
        </div>
        </div>
        
        <div class="tipbtn">
        <input name="" type="button"  class="sure" value="确定" />&nbsp;
        <input name="" type="button"  class="cancel" value="取消" />
        </div>
    
    </div>
    
    
    
    
    </div>
    
    <script type="text/javascript">
	$('.tablelist tbody tr:odd').addClass('odd');
	</script>
</body>
</html>
