<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>无标题文档</title>
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css" />
</head>

<body>

	<div class="place">
    <span>位置：</span>
    <ul class="placeul">
    <li><a href="#">首页</a></li>
    <li><a href="#">表单</a></li>
    </ul>
    </div>
    
    <div class="formbody">
    
    <div class="formtitle"><span>基本信息</span></div>
        @if(count($errors)>0)
            @foreach($errors as $v)
                <p style="background-color: yellow;">{{$v}}</p>
            @endforeach
        @endif
    <form name="edit" action="">
        <ul class="forminfo">
            <input name="id" type="hidden" value="{{$article['article_id']}}" class="dfinput" />
            <li><label>文章标题</label><input name="title" type="text" value="{{$article['title']}}" class="dfinput" /><i>标题不能超过30个字符</i></li>
            <li><label>文章链接</label><input name="url" type="text" value="{{$article['url']}}" class="dfinput" /><i>多个关键字用,隔开</i></li>
            <li><label>文章摘要</label><textarea name="summary" cols="" rows="" class="textinput">{{$article['summary']}}</textarea></li>
            <li><label>文章内容</label><textarea name="content" cols="" rows="" class="textinput">{{$article['content']}}</textarea></li>
            <li><label>创建时间</label><input name="create_time" type="text" value="{{$article['create_time']}}" class="dfinput"/></li>
            <li><label>&nbsp;</label><input name="" type="submit" class="btn" value="确认保存"/></li>
        </ul>
    </form>

    
    
    </div>
</body>
</html>
