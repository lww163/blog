<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class Login extends Model
{
    public $table='user';
    public $timestamps=false;
    public $fillable=['user_id','user_name','user_pass'];

}
