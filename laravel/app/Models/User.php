<?php
namespace App\Models;
use Illuminate\Database\Eloquent\Model;


class User extends Model
{
    public $table='user';
    public $timestamps=false; //在默认情况下，在数据库表里需要有 updated_at 和 created_at 两个字段。如果您不想设定或自动更新这两个字段，则将类里的 $timestamps 属性设为 false即可。
    public $fillable=['user_id','user_name','user_pass'];//允许批量赋值的字段。
    //  protected $guarded = ['article_id'];//是作为「黑名单」
}
