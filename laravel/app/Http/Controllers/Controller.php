<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesResources;

//用于权限
use  App\Models\User;  //获取登录用户表的信息
use  App\Models\Userrole;//获取登录用户的角色id的信息
use  App\Models\Role;  //获取角色表的信息
use  App\Models\Roleauth;//获取角色的权限ids的角色id的信息
use  App\Models\Permission;  //获取权限的信息


class Controller extends BaseController
{
    use AuthorizesRequests, AuthorizesResources, DispatchesJobs, ValidatesRequests;

    public function __construct()
    {
        //将权限设置到中间件
        $this->middleware('admin.permission');
    }
}
