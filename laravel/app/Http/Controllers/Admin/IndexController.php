<?php
namespace App\Http\Controllers\Admin;
use Illuminate\Support\Facades\DB;
use  Illuminate\Support\Facades\Input; //获取表单数据
use  App\Models\Login;  //获取登录用户表的信息
use  App\Http\Controllers\Controller;
use App\Http\Middleware\AdminPermission;

class IndexController extends Controller
{
    /*
     * 后台页面展示 上 左右格局
     *
     */
    public function index()
    {
        //echo '123456';
        return view('/admin/main');
    }

    public function top()
    {
        return view('/admin/top');
    }

    public function left()
    {
        //判断权限
        $userdata =session('userinfo');
        if($userdata){
            //dd($userdata);


           // $data = $this->getauths($userdata['user_id']);
            $new =new AdminPermission(); //调用中间件的方法
            $data =  $new->getallinfo();
            $authdata = $data['authdata'];
            if(count($authdata) > 0){
                foreach($authdata as $k=> $v){
                    if($v['pid']==0){ //顶级权限
                        $authA[] = $v;
                    }else{
                        $authB[] = $v;
                    }

                }
            }

        }else{
          echo '请登录后在访问';
            exit;
        }

        $data=[
            'userdata'=>$data['userdata'],
            'authA'=>$authA,
            'authB'=>$authB
        ];
       // var_dump($data);
        return view('/admin/left',['data'=>$data]);
    }

    public function right()
    {
        return view('/admin/right');
    }
    /*
    * 后台欢迎页面
    */
    public function welcome()
    {
        return view('/admin/welcome');
    }

}
