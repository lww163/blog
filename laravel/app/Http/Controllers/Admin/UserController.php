<?php
namespace App\Http\Controllers\Admin;



use Illuminate\Support\Facades\DB;
use  Illuminate\Support\Facades\Input; //获取表单数据
use  App\Models\Login;  //获取登录用户表的信息
use  App\Models\User;  //获取登录用户表的信息
use  App\Models\Userrole;//获取登录用户的角色id的信息
use  App\Models\Role;  //获取角色表的信息
use  App\Models\Roleauth;//获取角色的权限ids的角色id的信息
use  App\Models\Permission;  //获取权限的信息
use  App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;  //用于验证表单
use App\Http\Middleware\AdminPermission;
class UserController extends Controller
{

    /*
     * 用户列表页
     */
    public function userlist()
    {
        $data = User::all()->toArray();
        return view('/user/list',['data'=>$data]);
    }
    /*
     * 权限列表列表页
     */
    public function showauths($id)
    {
        $new =new AdminPermission();
        $data =  $new->getallinfo();
        $authdata = $data['authdata'];
        if(count($authdata) > 0){
            foreach($authdata as $k=> $v){
                if($v['pid']==0){ //顶级权限
                    $authA[] = $v;
                }else{
                    $authB[] = $v;
                }

            }
        }
        $data=[
            'userdata'=>$data['userdata'],
            'authA'=>$authA,
            'authB'=>$authB
        ];

        return view('permission/list',['data'=>$data]);
    }
    public function add()
    {

        echo '用户增加 -- 角色';
       // return view('/user/list',['data'=>$data]);
    }

}
