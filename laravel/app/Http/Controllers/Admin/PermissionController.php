<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\DB;
use  Illuminate\Support\Facades\Input; //获取表单数据
use  App\Models\Article;  //获取文章表的信息
use  App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;  //用于验证表单
//use  App\Http\Requests\Request;


class PermissionController extends Controller
{

    /*
     * 文章列表页
     */
    public function plist()
    {
        echo 'okok';exit;
        //获取当前用户
        $userinfo ='';
        $count ='';
        if($userinfo =session('userinfo')){
            //dd($userinfo);
            $count = Article::where('user_id',$userinfo['user_id'])->orderBy("create_time",'desc')->count();
            //$articledata = Article::where('user_id',$userinfo['user_id'])->orderBy("article_id",'desc')->get();
            //$articledata=$articledata->toArray();
            // $articledata = Article::where('user_id',$userinfo['user_id'])->orderBy("article_id",'desc')->paginate(1);
            $articledata = Article::where('user_id',$userinfo['user_id'])->orderBy("article_id",'desc')
                ->simplePaginate(5);
        }else{
            //展示所有的文章列表 ，包含不同的管理员，这种不会出现
            $articledata = Article::all()->toArray();
        }
        $data=[
            'article'=>$articledata,
            'user'=>$userinfo,
            'count'=>$count
        ];
        return view('/admin/list',['data'=>$data]);
    }
    /*
     * 文章编辑页
     */
    public function edit($id)
    {
        //搜索对应的文章信息
        $articledata = Article::where('article_id',$id)->first()->toArray();
        //  if($data = Input::all()){
        if($data = Input::except('_token')){
            //验证表单
            $rules =['title'=>'required|max:30','url'=>'url','summary'=>'min:5'];
            $message=['title.required'=>'文章标题不能为空！！','title.max'=>'文章标题长度不能超过30！！',
                'url.url'=>'文章链接必须合法的！！','summary.min'=>'文章摘要长度不能小于5！！'];
            $validator = Validator::make($data,$rules,$message);
            // var_dump($validator);
            if($validator->fails()){
                return redirect()->back()->with('errors',$validator->errors()->all());
            };
            //表单验证end
            Article::where('article_id', $id)
                ->update(array('title' => $data['title'],'url' => $data['url'],'summary' => $data['summary'],'content' => $data['content'],'create_time' => $data['create_time']));

            $articledata = Article::where('article_id',$id)->first()->toArray();
            return redirect('/article/list');
        }else{
            return view('/admin/edit',['article'=>$articledata]);
        }

    }
    /*
     * 文章增加动作
     *
     */
    public function add()
    {
        $userinfo =session('userinfo');
        if($data = Input::except('_token')){
            //验证表单
            $rules =['title'=>'required|max:30','url'=>'url','summary'=>'min:5'];
            $message=['title.required'=>'文章标题不能为空！！','title.max'=>'文章标题长度不能超过30！！',
                'url.url'=>'文章链接必须合法的！！','summary.min'=>'文章摘要长度不能小于5！！'];
            $validator = Validator::make($data,$rules,$message);
            // var_dump($validator);
            if($validator->fails()){
                return redirect()->back()->with('errors',$validator->errors()->all());
            };
            //表单验证end
            // Article::insert(array('title' => $data['title'],'user_id'=>$userinfo['user_id'],'url' => $data['url'],'summary' => $data['summary'],'content' => $data['content'],'create_time' => $data['create_time']));
            Article::insert($data);
            return redirect('/article/articlelist')->with('message','增加数据成功！');
        }else{
            return view('/admin/add',['userinfo'=>$userinfo]);
        }

    }
    /*
     * 文章删除动作
     *
     */
    public function delete($id)
    {

        Article::where('article_id',$id)->delete();

        return redirect('/article/articlelist')->with('message','删除数据成功！');
    }




}
