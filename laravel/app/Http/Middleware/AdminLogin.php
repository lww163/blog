<?php

namespace App\Http\Middleware;

use Closure;


class AdminLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request,Closure $next)
    {
        if (!session('userinfo')) {
           return redirect('admin/login');
        }
        return $next($request);
    }
}
