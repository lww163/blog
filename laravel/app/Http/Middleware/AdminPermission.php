<?php

namespace App\Http\Middleware;

use Closure;
//用于权限
use  App\Models\User;  //获取登录用户表的信息
use  App\Models\Userrole;//获取登录用户的角色id的信息
use  App\Models\Role;  //获取角色表的信息
use  App\Models\Roleauth;//获取角色的权限ids的角色id的信息
use  App\Models\Permission;  //获取权限的信息



class AdminPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request,Closure $next)
    {

        // 默认权限开启，不需要设定
        $default_auths=['login-login','index-index','index-top','index-right','index-left','login-loginout'];
        //不允许访问自己权限以外的路由
        $current_ca =$this->getcurrentname(); //当前控制器-方法 article-articlelist
        $allow_auth=[];
        $alldata =$this->getallinfo();  //用户数据 角色数据 权限数据
        if(count($alldata)>0){
            $auth_data =$alldata['authdata']; //权限数据
            //处理下格式
            foreach($auth_data as $k=>$v){
                if($v['name_c'] && $v['name_a']){
                    $allow_auth[] = $v['name_c'].'-'.$v['name_a'];
                }
            }
          /*  echo $current_ca;
              echo '<br/>';
              var_dump($allow_auth);*/
        }


        if(in_array($current_ca,$allow_auth)|| in_array($current_ca,$default_auths)){
           // echo '有权限权限访问';
           // exit;
        }else{
           // echo '无权限访问';
            exit;
        }

        return $next($request);
    }

    public function getcurrentname()
    {
        /*
         * 获取当前用户访问的路由信息
         * */
        $name=\Route::current()->getActionName();//App\Http\Controllers\Admin\ArticleController@articlelist
        $cm =strrchr($name,'\\');
        $cm =substr($cm,1);
        $cm_arr =explode('@',$cm);
        $controller_name= $cm_arr[0];//ArticleController
        $action_name= $cm_arr[1];    //articlelist
        //处理下获取的路由
        $controller_name = str_replace('Controller','',$controller_name);//Article
        $controller_name = strtolower($controller_name);
        return $controller_name.'-'.$action_name;
        /*echo $controller_name.'<br/>';
        echo $action_name;*/

    }

    public function getallinfo()
    {
        /*
         * 获取当面用户所有的 用户信息 角色信息 权限信息
         * @params  用户id
         * @return userdata roledata authdata
         */
        $data=[];
        $userdata = session('userinfo');
        if($userdata && count($userdata)>0){
            $id=$userdata['user_id'];
            //获取当前用户的信息
            $userdata = User::where('user_id',$id)->first()->toArray();
            //搜索该用户对应的角色id
           $roleid =  Userrole::where('user_id',$userdata['user_id'])->first()->toArray();
            // $roleid =  Userrole::where('user_id',$userdata['user_id'])->get();

            //搜索该用户对应的角色信息
            $roledata =  Role::where('role_id',$roleid['role_id'])->first()->toArray();

            $roledata =  Role::where('role_id',$roleid['role_id'])->first();
            if($roledata){
                $roledata= $roledata->toArray();
            }else{
                echo"请先给角色分布权限在操作";
            }
            //搜索该用户对应的权限ids


            $auths =  Roleauth::where('role_id',$roledata['role_id'])->first()->toArray();
            //搜索该用户对应的角色信息 auth_id=> 1,2,3
            $auth_arr = explode(',',$auths['auth_id']);
            $authdata =[];
            // dd($auth_arr);
            if($userdata['user_name'] == 'root'){
                $authdata=Permission::all()->toArray();
            }else{
                foreach($auth_arr as $k=> $v){
                    $authdata[] =  Permission::where('id',$v)->first()->toArray();
                }
            }
            // var_dump($authdata);


            $data=[
                'userdata'=>$userdata,
                'roledata'=>$roledata,
                'authdata'=>$authdata
            ];
        }

        return $data;

    }
}
