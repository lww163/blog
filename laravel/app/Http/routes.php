<?php


Route::group(['middleware' => []], function()
{
   // Route::get('/', function(){ return view('admin/login'); });

    Route::any('admin/login',  'Admin\LoginController@login');
    Route::any('admin/register',  'Admin\LoginController@register');


    // Route::get('admin/login',  'Admin\LoginController@login');
   // Route::post('admin/login',  'Admin\LoginController@login');

    //以下需要登录权限
    Route::get('admin/index',  'Admin\IndexController@index');
    Route::get('admin/top',  'Admin\IndexController@top');
    Route::get('admin/left',  'Admin\IndexController@left');
    Route::get('admin/right',  'Admin\IndexController@right');
    Route::get('admin/welcome',  'Admin\IndexController@welcome');


    Route::get('admin/loginout',  'Admin\LoginController@loginout');

    //操作文章的路由
    Route::get('article/articlelist',  'Admin\ArticleController@articlelist');
    Route::any('article/add',  'Admin\ArticleController@add');
    Route::any('article/edit/{id?}','Admin\ArticleController@edit');
    Route::get('article/delete/{id?}','Admin\ArticleController@delete');
    //操作用户的路由
    Route::get('user/userlist',  'Admin\UserController@userlist');
    Route::get('user/add',  'Admin\UserController@add');
    Route::any('user/showauths/{id}',  'Admin\UserController@showauths');
    //操作权限的路由
    Route::get('permission/plist',  'Admin\PermissionController@plist');
    Route::any('permission/add',  'Admin\PermissionController@add');
    Route::any('permission/edit/{id?}','Admin\PermissionController@edit');
    Route::get('permission/delete/{id?}','Admin\PermissionController@delete');




});
Route::group(['middleware' => ['admin.login']], function()
{
    //操作文章的路由
    Route::get('article/articlelist',  'Admin\ArticleController@articlelist');
  /*  //以下需要登录权限
    Route::get('admin/index',  'Admin\IndexController@index');
    Route::get('admin/top',  'Admin\IndexController@top');
    Route::get('admin/left',  'Admin\IndexController@left');
    Route::get('admin/right',  'Admin\IndexController@right');
    Route::get('admin/welcome',  'Admin\IndexController@welcome');


    Route::get('admin/loginout',  'Admin\LoginController@loginout');

    //操作文章的路由
    Route::get('article/list',  'Admin\ArticleController@articlelist');
    Route::any('article/add',  'Admin\ArticleController@articleadd');
    Route::any('article/edit/{id}','Admin\ArticleController@articleedit');
    Route::get('article/delete/{id}','Admin\ArticleController@delete');*/

});



